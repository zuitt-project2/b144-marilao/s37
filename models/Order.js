const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required:true
	},
	totalAmount: {
			type: Number,
			required: [true, 'Total is required']
			},
	purchasedOn: {
			type: Date,
			default: new Date()
			},
	productsPurchased: [
			{
				productId: {
				type: String,
				required: [true, 'ProductId is required']				
			},
				price: {
				type: Number,
				required:true
			},
				quantity: {
				type: Number,
				default: 1
			}
			
		}]
	})
module.exports = mongoose.model('Order', orderSchema);


		