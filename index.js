const express = require('express')
const mongoose = require('mongoose')
// Allows us to control app's Cross Origin Resource Sharing settings
const cors = require('cors');

//Routes
const userRoutes = require('./routes/user')

const productRoutes = require('./routes/product');

const orderRoutes = require('./routes/order')

// Server Setup
const app = express()
const port = 3000;
// allows all resources/origin to access our backend application
// Enable all CORS
app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended : true}))

//Defines the 'api/users' string to be included for all routes defined in the 'user' route file
app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)
app.use('/api/orders', orderRoutes)


// Database connection
mongoose.connect("mongodb+srv://Shiela1028:JesusChrist777@wdc028-course-booking.tflhi.mongodb.net/ecommerce_api?retryWrites=true&w=majority", {
	// to avoid/prevents future error in our connection to MongoDB
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("Now connected to MongoDB Atlas."))




app.listen(process.env.PORT || port, () => console.log(`Now listening to port ${process.env.PORT || port}`));