const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

const { response } = require('express')
const { findById } = require('../models/Product')

// Create order (NON-Admin only)

module.exports.createOrder = async (reqBody, userData) => {


    let totalAmount = 0 
    let productsPurchased = []

     runLoop = async () => {
        let total = 0

        for (let i = 0; i < reqBody.productsPurchased.length; i++ ) {

          await new Promise( resolve => setTimeout( resolve ) )

            await Product.findById(reqBody.productsPurchased[i].productId).then(result => {            
            
                if(reqBody.productsPurchased[i].quantity != undefined) {
                    
                    let quantityCalculate = result.price * reqBody.productsPurchased[i].quantity
                    console.log(reqBody.productsPurchased[i].quantity)
                    total = quantityCalculate + total
                    productsPurchased.push({productId : result.id, price : result.price, quantity: reqBody.productsPurchased[i].quantity})
                } else { 
                    total = result.price + total
                    productsPurchased.push({productId : result.id, price : result.price})
                }
                    
               
                if (i == reqBody.productsPurchased.length - 1) {
                    totalAmount = total
                }
            })
  
        }
  
        return true 
    }

    let isForLoopDone = await runLoop(); 

    
    if(isForLoopDone) {

     
        return User.findById(userData.id).then(result => {

            if (userData.isAdmin) { 
                return Promise.reject('Admin has no access in this area!');
            } else {

                
                let newOrder = new Order({
                    totalAmount: parseInt(totalAmount),
                    userId: userData.id,
                    productsPurchased: productsPurchased      
                })
                
                
                return newOrder.save().then((order, error) => {
                    return error ? false : 'Product added successfully!';
                }) 
            }
        }) 
    } else {
        
        return "error"
    }   
 }

// retrieve all orders(admin only)
module.exports.getAllOrders = (userData) => {
    
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {
            return Order.find({}).then(result => {
                return result
            })
        } else {
            return Promise.reject('Not authorized to access this page');
        }
    })
}

//  Retrieve Authenticated User's Orders (NON-admin only)
module.exports.getMyOrders = (userData) => {
    
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {
            return Promise.reject('Not authorized to access this page');
            
        } else {

            return Order.find({userId: userData.id}).then(result => {
                return result
            })
            
        }
    })
}