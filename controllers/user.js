const User = require('../models/User');
const Product = require('../models/Product');
/*const Order = require('../models/Order');*/
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) =>{
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		} else{
			//no duplicate email found
			return false;
		}
	})
}
//1.User Registration
module.exports.registerUser = (reqBody) => {
	 
			let newUser = new User(
			{
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})
			//save
			return newUser.save().then((user, error) =>{
				//if registration failed
				if(error){
					return false;
				}else{
					//User registration is successful
					return true
					}
				})
			}
		
//2.User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {
		//User does not exist
		if(result == null){
			return false;
		}else{
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			//If the password match/result of the above code is true
			if(isPasswordCorrect){
			
				return { accessToken: auth.createAccessToken(result.toObject())}
			}else{
				//Password do not match
				return false
			}
		}
	})
}

module.exports.setAsAdmin = (reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {
            
            let newUserType = {
                isAdmin: true
            }
        
            return User.findByIdAndUpdate(reqParams.userId, newUserType).then((user, error) => {
                if(error) {
                    return false
                } else {

                    let newUserPreview = {
                        email: user.email,
                        isAdmin: true
                    }
                    return newUserPreview
                }
            })

        } else {
            return Promise.reject('Not authorized to access this page');
        }
        
    });    
}

