const Product = require("../models/Product");
/*const Order = require('../models/Order');*/
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Creating a product(Admin only)


module.exports.addProduct = (reqBody, userData) => {

    return Product.findById(userData.userId).then(result => {

        if (userData.isAdmin !== true) {
            return "You are not an admin."
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newProduct.save().then((product, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Product added successfully!"
                }
            })
        }
        
    });    
}


//4.Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
};

// Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update a product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a product
module.exports.archiveProduct = (reqParams) => {
	let updatedActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedActiveField).then((product, error) => {

		if (error){
			return false;
		}
		else{
			return true;
		}
	})
}
