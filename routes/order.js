const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");

// Create Order(Non-admin only)

router.post("/checkout", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.createOrder(req.body, userData).then(resultFromController => res.send(resultFromController))
})

// Retrieve all orders(Admin only)
router.get("/orders", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.getAllOrders(userData)
    .then(resultFromController => res.send(resultFromController))
})



//Retrieve Authenticated User's Orders (NON-admin only)
router.get("/myOrders", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.getMyOrders(userData)
    .then(resultFromController => res.send(resultFromController))
})





module.exports = router;