const express = require('express');
const router = express.Router();
const userController = require('../controllers/user')
const orderController = require('../controllers/order')
const auth = require("../auth");

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})


//1.Routes for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

//2.Routes for authenticating a user

router.post('/:userId', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

//Routes for set user as an admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.setAsAdmin(req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

// Retrieve all orders(Admin only)
router.get("/orders", auth.verify, (req, res) => {

    const productOrder = auth.decode(req.headers.authorization)

    orderController.getAllOrders(req.body, {productId: productOrder.id, isAdmin:productOrder.isAdmin}).then(resultFromController => res.send(resultFromController))
})


//Retrieve Authenticated User's Orders (NON-admin only)
router.get("/myOrders", auth.verify, (req, res) => {

    const myOrders = auth.decode(req.headers.authorization)

    orderController.getMyOrders(req.body, {userId: myOrders.id, isAdmin:myOrders.isAdmin}).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
