const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


// 4.Retrieve all active products
router.get("/", (req, res) =>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// 5.Retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// 6. Create Product (Admin only)

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

// 7. Update Product information (Admin only)
router.put("/:productId", auth.verify, (req, res) =>{
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// 8. Archive Product (Admin only)
router.put("/:productId/archive", auth.verify, (req,res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
